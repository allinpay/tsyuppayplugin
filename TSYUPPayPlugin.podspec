Pod::Spec.new do |s|

  s.name = "TSYUPPayPlugin"
  s.version = "0.1.0"
  s.license = "MIT"
  s.summary = "IOS版银联手机控件支付开发"
  s.homepage = "https://gitlab.com/allinpay/tsyuppayplugin"
  s.author       = { "Bell" => "bell@greedlab.com" }
  s.source       = { :git => "git@gitlab.com:allinpay/tsyuppayplugin.git", :tag => s.version }
  s.platform = :ios
  s.requires_arc = true
  s.frameworks = 'CFNetwork','SystemConfiguration','Security'
  s.libraries = 'z'
  s.source_files = 'paymentcontrol/inc/*.h'
  s.vendored_libraries = 'paymentcontrol/libs/libPaymentControl.a'

end
